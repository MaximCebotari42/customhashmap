public class Main {
    public static void main(String[] args){
        CustomHashMap<String, String> test = new CustomHashMap();

        test.put("testKey1", "testValue1");
        test.put("testKey2", "testValue2");
        test.put("testKey3", "testValue3");
        test.put("testKey4", "testValue4");
        test.put("testKey5", "testValue5");
        test.put("testKey6", "testValue6");
        test.put("testKey7", "testValue7");
        test.put("testKey8", "testValue8");
        test.put("testKey8", null);
        test.put(null, "fds");
        test.put(null, "dsfds");
        test.put(null, "fds");
        test.put(null, null);
        test.entrySet();
        test.put("testKey9", "testValue9");
        test.put("testKey10", "testValue10");
        test.put("testKey11", "testValue11");
        test.put("testKey12", "testValue12");
        test.put("testKey13", "testValue13");
    }
}
