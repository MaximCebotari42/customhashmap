import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import java.util.*;
import java.util.stream.Collectors;

public class CustomHashMap<K, V> implements Map<K, V> {

    private int bucketSizeFactor = 2;
    private int bucketListCapacity = 2 << bucketSizeFactor;
    private int elementsInBucket = 0;
    private final float occupancyThreshold = 0.7f; //The percentage at which the array should be expanded
    private float rehashingLimit = bucketListCapacity * occupancyThreshold;
    private Node<K, V>[] bucketList = (Node<K, V>[]) new Node[bucketListCapacity]; //last element added for null values

    @Override
    public int size() {
        return elementsInBucket;
    }

    @Override
    public boolean isEmpty() {
        return elementsInBucket == 0;
    }

    @Override
    public boolean containsKey(Object o) {
        return get(o) != null;
    }

    @Override
    public boolean containsValue(Object o) {
        try {
            return values().contains(o);
        } catch (Throwable t) {
            return false; //object is of different type
        }
    }

    @Override
    public V get(Object o) {
        if (o != null) {
            int objectIndex = getIndexForObject(o);
            if (bucketListCapacity >= objectIndex) {
                Node<K, V> node = bucketList[objectIndex]; //first node
                if (node != null) {
                    if (node.getKey().equals(o)) {
                        return node.getValue();
                    } else {
                        while (node.hasNext()) {
                            if (node.getNext().getKey().equals(o)) { //nextNode is the one
                                return node.getNext().getValue();
                            } else {
                                node = node.getNext();
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    @Nullable
    @Override
    public V put(K key, V value) {
        V returnValue = null;

        if (key == null) {
            Node<K, V> existingNullNode = bucketList[0];
            if (existingNullNode == null){ //no null element inserted in map before
                if (value != null){
                    elementsInBucket++;
                    bucketList[0] = new Node<>(null, value, null);
                } // no need for else, as put(null, null) should be delete existing value, but it's not the case here
            } else { //null element present
                if (value == null){ //should delete existing null node
                    returnValue = bucketList[0].getValue();
                    bucketList[0] = null;
                    elementsInBucket--;
                } else { // should replace existing null value
                    returnValue = existingNullNode.getValue();
                    existingNullNode.setValue(value);
                }
            }
            return returnValue;
        }

        if (entrySet().size() == (int) rehashingLimit) {
            rehashBucketList();
        }

        if (get(key) != null) {
            Node<K, V> node = bucketList[getIndexForObject(key)]; // the first node in the bucket
            if (node.getKey().equals(key)) {
                returnValue = node.getValue();
                if (value != null) {
                    node.setValue(value);
                } else {
                    elementsInBucket--;
                    bucketList[getIndexForObject(key)] = node.getNext();
                }
            } else {
                while (node.hasNext()) {
                    if (node.getNext().getKey().equals(key)) { //nextNode is the one
                        returnValue = node.getNext().getValue();
                        if (value != null) {
                            node.getNext().setValue(value);
                        } else {
                            elementsInBucket--;
                            node.setNext(node.getNext().getNext());
                        }
                    } else {
                        node = node.getNext();
                    }
                }
            }
        } else {
            int bucketIndex = getIndexForObject(key);
            bucketList[bucketIndex] = new Node<>(key, value, bucketList[bucketIndex]);
            elementsInBucket++;
            rehashingLimit = bucketListCapacity * occupancyThreshold;
        }
        return returnValue;
    }

    private void rehashBucketList() {
        bucketSizeFactor++;
        bucketListCapacity = 2 << bucketSizeFactor;
        Node<K, V>[] newBucketList = (Node<K, V>[]) new Node[bucketListCapacity];
        for (Node<K, V> kvNode : bucketList) {
            int bucketIndex = getIndexForObject(kvNode);
            newBucketList[bucketIndex] = kvNode;
        }
        bucketList = newBucketList;
    }

    private int getIndexForObject(Object o) {
        if (o == null){
            return 0;
        } else {
            return Math.abs(o.hashCode() % (bucketListCapacity)) + 1;// add +1 so that index should never be 0, as this is reserved for null
        }
    }

    @Override
    public V remove(Object o) {
        V removedValue = null;
        if (o != null) {
            int objectIndex = getIndexForObject(o);
            if (bucketListCapacity >= objectIndex) {
                Node<K, V> node = bucketList[objectIndex]; //first node
                if (node.getKey().equals(o)) {
                    if (node.hasNext()) {
                        bucketList[objectIndex] = node.getNext();
                    } else {
                        removedValue = bucketList[objectIndex].getValue();
                        bucketList[objectIndex] = null;
                    }
                } else {
                    while (node.hasNext()) {
                        if (node.getNext().getKey().equals(o)) { //nextNode is the one
                            removedValue = node.getNext().getValue();
                            node.setNext(node.getNext().getNext()); //skip the node that has to be removed
                        } else {
                            node = node.getNext();
                        }
                    }
                }
            }
        } else {
            removedValue = bucketList[bucketListCapacity].getValue();
            bucketList[bucketListCapacity] = null;
        }
        elementsInBucket--;
        return removedValue;
    }

    @Override
    public void putAll(@NotNull Map<? extends K, ? extends V> map) {
        for (Entry<? extends K, ? extends V> setToAdd : map.entrySet()) {
            put(setToAdd.getKey(), setToAdd.getValue());
        }
    }

    @Override
    public void clear() {
        elementsInBucket = 0;
        bucketList = (Node<K, V>[]) new Node[bucketListCapacity + 1];
    }

    @NotNull
    @Override
    public Set<K> keySet() {
        return entrySet()
                .stream()
                .map(Entry::getKey)
                .collect(Collectors.toSet());
    }

    @NotNull
    @Override
    public Collection<V> values() {
        return entrySet()
                .stream()
                .map(Entry::getValue)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> response = new HashSet<>();
        for (Node<K, V> index : bucketList) {
            if (index != null) {
                response.add(index);
                while (index.hasNext()) {
                    response.add(index.getNext());
                    index = index.getNext();
                }
            }
        }
        return response;
    }

    private static class Node<K, V> implements Map.Entry<K, V> {

        private Node(K key, V value, Node<K, V> next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }

        @Override
        public int hashCode() {
            if(key != null) { return key.hashCode();} else { return 0;}
        }

        private final K key;
        private V value;
        private Node<K, V> next;

        @Override
        public K getKey() {
            return key;
        }


        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            V returnValue = value;
            this.value = value;
            return returnValue;
        }

        boolean hasNext() {
            return next != null;
        }

        Node<K, V> getNext() {
            return next;
        }

        void setNext(Node<K, V> next) {
            this.next = next;
        }
    }
}


